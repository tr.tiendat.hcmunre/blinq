export const state = {
  brands: [],
  canLoadMore: true,
  currentPage: 0,
  totalPage: 0,
  currentCount: 0,
  totalCount: 0,
};
