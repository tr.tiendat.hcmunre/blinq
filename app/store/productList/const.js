export const MODULE_NAME = 'product-list';

/* GETTERS - START */
const _GETTERS = {
  PRODUCTS: 'products',
  CAN_LOADMORE: 'canLoadMore',
  PAGE: 'currentPage',
  TOTAL_PAGE: 'totalPage',
  COUNT: 'currentCount',
  TOTAL_COUNT: 'totalCount',
  FILTER_TARGET: 'filterTarget',
  FILTER_BRAND: 'filterBrand',
  FILTER_CATEGORY: 'filterCategory',
};

export const GETTERS = {
  PRODUCTS: `${MODULE_NAME}/${_GETTERS.PRODUCTS}`,
  CAN_LOADMORE: `${MODULE_NAME}/${_GETTERS.CAN_LOADMORE}`,
  PAGE: `${MODULE_NAME}/${_GETTERS.PAGE}`,
  TOTAL_PAGE: `${MODULE_NAME}/${_GETTERS.TOTAL_PAGE}`,
  COUNT: `${MODULE_NAME}/${_GETTERS.COUNT}`,
  TOTAL_COUNT: `${MODULE_NAME}/${_GETTERS.TOTAL_COUNT}`,
  FILTER_TARGET: `${MODULE_NAME}/${_GETTERS.FILTER_TARGET}`,
  FILTER_BRAND: `${MODULE_NAME}/${_GETTERS.FILTER_BRAND}`,
  FILTER_CATEGORY: `${MODULE_NAME}/${_GETTERS.FILTER_CATEGORY}`,
};
/* GETTERS - END */

/* ACTIONS - START */
const _ACTIONS = {
  LOAD_TRENDING_PRODUCTS: 'loadTrendingProducts',
  LOAD_PRELOVED_PRODUCTS: 'loadPrelovedProducts',
  LOAD_ASEANHOUZ_PRODUCTS: 'loadAseanHouzProducts',
  LOAD_ONSALE_PRODUCTS: 'loadOnSaleProducts',
  LOAD_CATEGORY_PRODUCTS: 'loadCategoryProducts',
  LOAD_BRAND_PRODUCTS: 'loadBrandProducts',
  LOAD_TARGET_PRODUCTS: 'loadTargetProducts',
};

export const ACTIONS = {
  LOAD_TRENDING_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_TRENDING_PRODUCTS}`,
  LOAD_PRELOVED_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_PRELOVED_PRODUCTS}`,
  LOAD_ASEANHOUZ_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_ASEANHOUZ_PRODUCTS}`,
  LOAD_ONSALE_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_ONSALE_PRODUCTS}`,
  LOAD_CATEGORY_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_CATEGORY_PRODUCTS}`,
  LOAD_BRAND_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_BRAND_PRODUCTS}`,
  LOAD_TARGET_PRODUCTS: `${MODULE_NAME}/${_ACTIONS.LOAD_TARGET_PRODUCTS}`,
};
/* ACTIONS - END */

/* MUTATIONS - START */
const _MUTATORS = {
  SET_PRODUCTS: 'setProducts',
  CLEAR_PRODUCTS: 'clearProducts',
  ADD_MORE_PRODUCTS: 'addMoreProducts',
  SET_PAGING: 'setPaging',
  SET_FILTERS: 'setFilters',
  CLEAR_FILTERS: 'clearFilters',
};

export const MUTATORS = {
  CLEAR_PRODUCTS: `${MODULE_NAME}/${_MUTATORS.CLEAR_PRODUCTS}`,
  CLEAR_FILTERS: `${MODULE_NAME}/${_MUTATORS.CLEAR_FILTERS}`,
};
/* MUTATIONS - END */

export default {
  _GETTERS,
  _ACTIONS,
  _MUTATORS,
};
