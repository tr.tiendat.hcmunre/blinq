export const state = {
  products: [],
  canLoadMore: true,
  currentPage: 0,
  totalPage: 0,
  currentCount: 0,
  totalCount: 0,
  filters: {
    targets: [],
    brands: [],
    categories: [],
  },
};
