/* GETTERS - START */
export const GETTERS = {
  PROCESSING: 'processing',
  APP_CONFIG: 'getAppCoreConfig',
};
/* GETTERS - END */

/* ACTIONS - START */
export const ACTIONS = {
  LOAD_APP_CONFIG: 'loadAppCoreConfig',
};
/* ACTIONS - END */

/* MUTATIONS - START */
export const MUTATORS = {
  SET_APP_CONFIG: 'setAppCoreConfig',
  ADD_PROCESSING: 'addProcessingTask',
  REMOVE_PROCESSING: 'removeProcessingTask',
};
/* MUTATIONS - END */
