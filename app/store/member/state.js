import { getSetting } from "~/common/helpers/storage/deviceStorage.helper";

export const state = {
  memberId: getSetting('memberId') || null,
  memberName: getSetting('memberName') || null,
  loginError: null,
  profile: {},
  registerResult: null,
  forgotPswResult: null,
  modelPhotos: [],
  modelPhotoItem: null,
  addresses: [],
  orders: [],
  modelPhotoChange: 0,
  addressChange: 0,
};
