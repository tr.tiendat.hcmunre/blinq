export const state = {
  banners: [],
  tickers: [],
  miniBanners: [],
  featuredProducts: [],
  brands: [],
  blogHighlights: [],
};
