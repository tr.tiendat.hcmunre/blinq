import { ATTRIBUTE_STATUS } from "~/common/constants";

export const state = {
  product: {},
  attributeDetail: {
    status: ATTRIBUTE_STATUS.NOT_AVAILABLE,
  },
};
