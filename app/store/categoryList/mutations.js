import CONST from './const';

export const mutations = {
  [CONST._MUTATORS.CLEAR_CATEGORIES]: (state) => {
    state.categories = [];
  },
  [CONST._MUTATORS.SET_CATEGORIES]: (state, categories) => {
    state.categories = categories;
  },
  [CONST._MUTATORS.ADD_MORE_CATEGORIES]: (state, categories) => {
    console.log("STATE CHANGED: ", state.categories.length, "--", categories.length);
    
    state.categories = [
      ...state.categories,
      ...categories,
    ];
  },
  [CONST._MUTATORS.SET_PAGING]: (state, { page, perPage, totalPage, count, totalCount}) => {
    state.currentPage = page;
    state.totalPage = totalPage;
    state.currentCount = page === 1 ? count : (state.currentCount + count);
    state.totalCount = totalCount;
    if (count < perPage) {
      state.canLoadMore = false;
    } else {
      state.canLoadMore = (state.currentCount < state.totalCount);
    }
  },
};
