import CONST from './const';
import {
  getProductCategories,
} from '../../services';

const parseContent = (content, count) => {
  const items = [];
  for (let index = 0; index < count; index++) {
    items.push(content[`${index}`]);
  }
  return items;
};

export const actions = {
  [CONST._ACTIONS.LOAD_CATEGORIES]: async ({ commit }, {
    page = 1,
    perPage = 8,
  }) => {
    try {
      const response = await getProductCategories(page, perPage);
      console.log("RESPONSE: ", response);
      
      const { Content, Count, TotalPages, TotalCount } = response;
      const items = parseContent(Content, Count);
      if (page === 1) {
        commit(CONST._MUTATORS.SET_CATEGORIES, items);
      } else {
        commit(CONST._MUTATORS.ADD_MORE_CATEGORIES, items);
      }
      commit(CONST._MUTATORS.SET_PAGING, {
        page,
        perPage,
        totalPage: TotalPages,
        count: Count,
        totalCount: TotalCount,
      });
    } catch (error) {
      console.log('error', error);
    }
  },
};
