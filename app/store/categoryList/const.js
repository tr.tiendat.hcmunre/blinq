export const MODULE_NAME = 'categoryList';

/* GETTERS - START */
const _GETTERS = {
  CATEGORIES: 'categories',
  CAN_LOADMORE: 'canLoadMore',
  PAGE: 'currentPage',
  TOTAL_PAGE: 'totalPage',
  COUNT: 'currentCount',
  TOTAL_COUNT: 'totalCount',
};

export const GETTERS = {
  CATEGORIES: `${MODULE_NAME}/${_GETTERS.CATEGORIES}`,
  CAN_LOADMORE: `${MODULE_NAME}/${_GETTERS.CAN_LOADMORE}`,
  PAGE: `${MODULE_NAME}/${_GETTERS.PAGE}`,
  TOTAL_PAGE: `${MODULE_NAME}/${_GETTERS.TOTAL_PAGE}`,
  COUNT: `${MODULE_NAME}/${_GETTERS.COUNT}`,
  TOTAL_COUNT: `${MODULE_NAME}/${_GETTERS.TOTAL_COUNT}`,
};
/* GETTERS - END */

/* ACTIONS - START */
const _ACTIONS = {
  LOAD_CATEGORIES: 'loadCategories',
};

export const ACTIONS = {
  LOAD_CATEGORIES: `${MODULE_NAME}/${_ACTIONS.LOAD_CATEGORIES}`,
};
/* ACTIONS - END */

/* MUTATIONS - START */
const _MUTATORS = {
  SET_CATEGORIES: 'setCategories',
  CLEAR_CATEGORIES: 'clearCategories',
  ADD_MORE_CATEGORIES: 'addMoreCategories',
  SET_PAGING: 'setPaging',
};

export const MUTATORS = {
  CLEAR_CATEGORIES: `${MODULE_NAME}/${_MUTATORS.CLEAR_CATEGORIES}`,
};
/* MUTATIONS - END */

export default {
  _GETTERS,
  _ACTIONS,
  _MUTATORS,
};
