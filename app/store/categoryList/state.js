export const state = {
  categories: [],
  canLoadMore: true,
  currentPage: 0,
  totalPage: 0,
  currentCount: 0,
  totalCount: 0,
};
