import CONST from './const';
import { getWishListProducts, addOrRemoveWishlistProduct } from '../../services/product';

const parseContent = (content, count) => {
  const items = [];
  for (let index = 0; index < count; index++) {
    items.push(content[`${index}`]);
  }
  // const { TargetList, BrandList, CategoryList } = content;
  // delete TargetList.count;
  // const targets = Object.values(TargetList);
  // delete BrandList.count;
  // const brands = Object.values(BrandList);
  // delete CategoryList.count;
  // const categories = Object.values(CategoryList);
  return {
    items,
    // targets, brands, categories
  };
};

export const actions = {
  [CONST._ACTIONS.LOAD_WISHLIST_PRODUCT]: async ({ commit }, memberId) => {
    try {
      // commit(CONST._MUTATORS.CLEAR_PRODUCTS);
      const response = await getWishListProducts(memberId);
      const { Content, Count } = response;
      const { items } = parseContent(Content, Count);
      // if (page === 1) {
      //   commit(CONST._MUTATORS.SET_BRANDS, items);
      // } else {
      //   commit(CONST._MUTATORS.ADD_MORE_BRANDS, items);
      // }
      // commit(CONST._MUTATORS.SET_PAGING, {
      //   page,
      //   perPage,
      //   totalPage: TotalPages,
      //   count: Count,
      //   totalCount: TotalCount,
      // });
      commit(CONST._MUTATORS.SET_PRODUCTS, items)
    } catch (error) {
      console.log('error', error);
    }
  },
  [CONST._ACTIONS.ADD_REMOVE_WISHLIST_ITEM]: async ({ commit }, {
    memberId,
    productId,
    isDetailPage = true,
  }) => {
    try {      
      const response = await addOrRemoveWishlistProduct(memberId, productId);
      console.log("RESPONSE: ", JSON.stringify(response));
      return response
      // if (response.Status === "ADDED") {
      //   // Added product to wishlist
      // } else if (response.Status === "REMOVED") {
      //   // Removed product from wishlist
      // }
      
    } catch (error) {
      console.log('error', error);
      return error
    }
  },
};