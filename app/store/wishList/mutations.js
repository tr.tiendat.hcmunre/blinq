import CONST from './const';

export const mutations = {
  [CONST._MUTATORS.SET_PRODUCTS]: (state, products) => {
    state.wishList = products
  },
  [CONST._MUTATORS.CLEAR_PRODUCTS]: (state) => {
    state.wishList = [];
  },
};