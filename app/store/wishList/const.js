export const MODULE_NAME = "wish-list";

/* GETTERS - START */
const _GETTERS = {
  PRODUCTS: 'products',
  // CAN_LOADMORE: 'canLoadMore',
  // PAGE: 'currentPage',
  // TOTAL_PAGE: 'totalPage',
  // COUNT: 'currentCount',
  // TOTAL_COUNT: 'totalCount',
  // FILTER_TARGET: 'filterTarget',
  // FILTER_BRAND: 'filterBrand',
  // FILTER_CATEGORY: 'filterCategory',
};

export const GETTERS = {
  PRODUCTS: `${MODULE_NAME}/${_GETTERS.PRODUCTS}`,
  // CAN_LOADMORE: `${MODULE_NAME}/${_GETTERS.CAN_LOADMORE}`,
  // PAGE: `${MODULE_NAME}/${_GETTERS.PAGE}`,
  // TOTAL_PAGE: `${MODULE_NAME}/${_GETTERS.TOTAL_PAGE}`,
  // COUNT: `${MODULE_NAME}/${_GETTERS.COUNT}`,
  // TOTAL_COUNT: `${MODULE_NAME}/${_GETTERS.TOTAL_COUNT}`,
  // FILTER_TARGET: `${MODULE_NAME}/${_GETTERS.FILTER_TARGET}`,
  // FILTER_BRAND: `${MODULE_NAME}/${_GETTERS.FILTER_BRAND}`,
  // FILTER_CATEGORY: `${MODULE_NAME}/${_GETTERS.FILTER_CATEGORY}`,
};
/* GETTERS - END */

/* ACTIONS - START */
const _ACTIONS = {
  LOAD_WISHLIST_PRODUCT: 'loadWishList',
  ADD_REMOVE_WISHLIST_ITEM: 'addRemoveWishlist'
};

export const ACTIONS = {
  LOAD_WISHLIST_PRODUCT: `${MODULE_NAME}/${_ACTIONS.LOAD_WISHLIST_PRODUCT}`,
  ADD_REMOVE_WISHLIST_ITEM: `${MODULE_NAME}/${_ACTIONS.ADD_REMOVE_WISHLIST_ITEM}`,
};
/* ACTIONS - END */

/* MUTATIONS - START */
const _MUTATORS = {
  SET_PRODUCTS: 'setWishListProducts',
  CLEAR_PRODUCTS: 'clearWishListProducts',
  // ADD_MORE_PRODUCTS: 'addMoreProducts',
  // SET_PAGING: 'setPaging',
  // SET_FILTERS: 'setFilters',
  // CLEAR_FILTERS: 'clearFilters',
};

export const MUTATORS = {
  CLEAR_PRODUCTS: `${MODULE_NAME}/${_MUTATORS.CLEAR_PRODUCTS}`,
  // CLEAR_FILTERS: `${MODULE_NAME}/${_MUTATORS.CLEAR_FILTERS}`,
};

export default {
  _GETTERS,
  _ACTIONS,
  _MUTATORS,
};