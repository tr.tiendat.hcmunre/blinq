export const VALIDATE_MSG = {
  REQUIRED: 'This field is required.',
  INVALID_EMAIL: 'Please input correct email.',
  INVALID_PASSWORD: 'Password must be at least 8 charactors.',
  INVALID_CONFIRM_PASSWORD: 'Confirm password does not match password.',
};
