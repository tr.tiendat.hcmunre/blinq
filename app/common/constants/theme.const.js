// const Color = require('tns-core-modules/color');

export const COLOR = {
  FONT_DEFAULT: '#030303',
  FONT_BTN_WHITE: '#F9F9F9',
  FONT_BTN_GREY: '#231f20',
  BG_DEFAULT: '#F8F8F8',
  PINK: '#EC0C88',
  PINK_DISABLED: '#F78BC6',
  GREY: '#E6E6E6',
  BTN_SHADOW_COLOR: '#DADADA',
  SHADOW_COLOR: '#e0e0e0',
  // BG_TOP_ICON: new Color(50, 255, 255, 255),
  BLACK: '#000',
};
