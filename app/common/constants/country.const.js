export const COUNTRIES = [
  {
    "name": "Afghanistan",
    "code": "AF",
    "flagsImage": "~/assets/images/flags/af.png"
  },
  {
    "name": "Åland Islands",
    "code": "AX",
    "flagsImage": "~/assets/images/flags/ax.png"
  },
  {
    "name": "Albania",
    "code": "AL",
    "flagsImage": "~/assets/images/flags/al.png"
  },
  {
    "name": "Algeria",
    "code": "DZ",
    "flagsImage": "~/assets/images/flags/dz.png"
  },
  {
    "name": "American Samoa",
    "code": "AS",
    "flagsImage": "~/assets/images/flags/as.png"
  },
  {
    "name": "AndorrA",
    "code": "AD",
    "flagsImage": "~/assets/images/flags/ad.png"
  },
  {
    "name": "Angola",
    "code": "AO",
    "flagsImage": "~/assets/images/flags/ao.png"
  },
  {
    "name": "Anguilla",
    "code": "AI",
    "flagsImage": "~/assets/images/flags/ai.png"
  },
  {
    "name": "Antarctica",
    "code": "AQ",
    "flagsImage": "~/assets/images/flags/aq.png"
  },
  {
    "name": "Antigua and Barbuda",
    "code": "AG",
    "flagsImage": "~/assets/images/flags/ag.png"
  },
  {
    "name": "Argentina",
    "code": "AR",
    "flagsImage": "~/assets/images/flags/ar.png"
  },
  {
    "name": "Armenia",
    "code": "AM",
    "flagsImage": "~/assets/images/flags/am.png"
  },
  {
    "name": "Aruba",
    "code": "AW",
    "flagsImage": "~/assets/images/flags/aw.png"
  },
  {
    "name": "Australia",
    "code": "AU",
    "flagsImage": "~/assets/images/flags/au.png"
  },
  {
    "name": "Austria",
    "code": "AT",
    "flagsImage": "~/assets/images/flags/at.png"
  },
  {
    "name": "Azerbaijan",
    "code": "AZ",
    "flagsImage": "~/assets/images/flags/az.png"
  },
  {
    "name": "Bahamas",
    "code": "BS",
    "flagsImage": "~/assets/images/flags/bs.png"
  },
  {
    "name": "Bahrain",
    "code": "BH",
    "flagsImage": "~/assets/images/flags/bh.png"
  },
  {
    "name": "Bangladesh",
    "code": "BD",
    "flagsImage": "~/assets/images/flags/bd.png"
  },
  {
    "name": "Barbados",
    "code": "BB",
    "flagsImage": "~/assets/images/flags/bb.png"
  },
  {
    "name": "Belarus",
    "code": "BY",
    "flagsImage": "~/assets/images/flags/by.png"
  },
  {
    "name": "Belgium",
    "code": "BE",
    "flagsImage": "~/assets/images/flags/be.png"
  },
  {
    "name": "Belize",
    "code": "BZ",
    "flagsImage": "~/assets/images/flags/bz.png"
  },
  {
    "name": "Benin",
    "code": "BJ",
    "flagsImage": "~/assets/images/flags/bj.png"
  },
  {
    "name": "Bermuda",
    "code": "BM",
    "flagsImage": "~/assets/images/flags/bm.png"
  },
  {
    "name": "Bhutan",
    "code": "BT",
    "flagsImage": "~/assets/images/flags/bt.png"
  },
  {
    "name": "Bolivia",
    "code": "BO",
    "flagsImage": "~/assets/images/flags/bo.png"
  },
  {
    "name": "Bosnia and Herzegovina",
    "code": "BA",
    "flagsImage": "~/assets/images/flags/ba.png"
  },
  {
    "name": "Botswana",
    "code": "BW",
    "flagsImage": "~/assets/images/flags/bw.png"
  },
  {
    "name": "Bouvet Island",
    "code": "BV",
    "flagsImage": "~/assets/images/flags/bv.png"
  },
  {
    "name": "Brazil",
    "code": "BR",
    "flagsImage": "~/assets/images/flags/br.png"
  },
  {
    "name": "British Indian Ocean Territory",
    "code": "IO",
    "flagsImage": "~/assets/images/flags/io.png"
  },
  {
    "name": "Brunei Darussalam",
    "code": "BN",
    "flagsImage": "~/assets/images/flags/bn.png"
  },
  {
    "name": "Bulgaria",
    "code": "BG",
    "flagsImage": "~/assets/images/flags/bg.png"
  },
  {
    "name": "Burkina Faso",
    "code": "BF",
    "flagsImage": "~/assets/images/flags/bf.png"
  },
  {
    "name": "Burundi",
    "code": "BI",
    "flagsImage": "~/assets/images/flags/bi.png"
  },
  {
    "name": "Cambodia",
    "code": "KH",
    "flagsImage": "~/assets/images/flags/kh.png"
  },
  {
    "name": "Cameroon",
    "code": "CM",
    "flagsImage": "~/assets/images/flags/cm.png"
  },
  {
    "name": "Canada",
    "code": "CA",
    "flagsImage": "~/assets/images/flags/ca.png"
  },
  {
    "name": "Cape Verde",
    "code": "CV",
    "flagsImage": "~/assets/images/flags/cv.png"
  },
  {
    "name": "Cayman Islands",
    "code": "KY",
    "flagsImage": "~/assets/images/flags/ky.png"
  },
  {
    "name": "Central African Republic",
    "code": "CF",
    "flagsImage": "~/assets/images/flags/cf.png"
  },
  {
    "name": "Chad",
    "code": "TD",
    "flagsImage": "~/assets/images/flags/td.png"
  },
  {
    "name": "Chile",
    "code": "CL",
    "flagsImage": "~/assets/images/flags/cl.png"
  },
  {
    "name": "China",
    "code": "CN",
    "flagsImage": "~/assets/images/flags/cn.png"
  },
  {
    "name": "Christmas Island",
    "code": "CX",
    "flagsImage": "~/assets/images/flags/cx.png"
  },
  {
    "name": "Cocos (Keeling) Islands",
    "code": "CC",
    "flagsImage": "~/assets/images/flags/cc.png"
  },
  {
    "name": "Colombia",
    "code": "CO",
    "flagsImage": "~/assets/images/flags/co.png"
  },
  {
    "name": "Comoros",
    "code": "KM",
    "flagsImage": "~/assets/images/flags/km.png"
  },
  {
    "name": "Congo",
    "code": "CG",
    "flagsImage": "~/assets/images/flags/cg.png"
  },
  {
    "name": "Congo, The Democratic Republic of the",
    "code": "CD",
    "flagsImage": "~/assets/images/flags/cd.png"
  },
  {
    "name": "Cook Islands",
    "code": "CK",
    "flagsImage": "~/assets/images/flags/ck.png"
  },
  {
    "name": "Costa Rica",
    "code": "CR",
    "flagsImage": "~/assets/images/flags/cr.png"
  },
  {
    "name": "Cote D\"Ivoire",
    "code": "CI",
    "flagsImage": "~/assets/images/flags/ci.png"
  },
  {
    "name": "Croatia",
    "code": "HR",
    "flagsImage": "~/assets/images/flags/hr.png"
  },
  {
    "name": "Cuba",
    "code": "CU",
    "flagsImage": "~/assets/images/flags/cu.png"
  },
  {
    "name": "Cyprus",
    "code": "CY",
    "flagsImage": "~/assets/images/flags/cy.png"
  },
  {
    "name": "Czech Republic",
    "code": "CZ",
    "flagsImage": "~/assets/images/flags/cz.png"
  },
  {
    "name": "Denmark",
    "code": "DK",
    "flagsImage": "~/assets/images/flags/dk.png"
  },
  {
    "name": "Djibouti",
    "code": "DJ",
    "flagsImage": "~/assets/images/flags/dj.png"
  },
  {
    "name": "Dominica",
    "code": "DM",
    "flagsImage": "~/assets/images/flags/dm.png"
  },
  {
    "name": "Dominican Republic",
    "code": "DO",
    "flagsImage": "~/assets/images/flags/do.png"
  },
  {
    "name": "Ecuador",
    "code": "EC",
    "flagsImage": "~/assets/images/flags/ec.png"
  },
  {
    "name": "Egypt",
    "code": "EG",
    "flagsImage": "~/assets/images/flags/eg.png"
  },
  {
    "name": "El Salvador",
    "code": "SV",
    "flagsImage": "~/assets/images/flags/sv.png"
  },
  {
    "name": "Equatorial Guinea",
    "code": "GQ",
    "flagsImage": "~/assets/images/flags/gq.png"
  },
  {
    "name": "Eritrea",
    "code": "ER",
    "flagsImage": "~/assets/images/flags/er.png"
  },
  {
    "name": "Estonia",
    "code": "EE",
    "flagsImage": "~/assets/images/flags/ee.png"
  },
  {
    "name": "Ethiopia",
    "code": "ET",
    "flagsImage": "~/assets/images/flags/et.png"
  },
  {
    "name": "Falkland Islands (Malvinas)",
    "code": "FK",
    "flagsImage": "~/assets/images/flags/fk.png"
  },
  {
    "name": "Faroe Islands",
    "code": "FO",
    "flagsImage": "~/assets/images/flags/fo.png"
  },
  {
    "name": "Fiji",
    "code": "FJ",
    "flagsImage": "~/assets/images/flags/fj.png"
  },
  {
    "name": "Finland",
    "code": "FI",
    "flagsImage": "~/assets/images/flags/fi.png"
  },
  {
    "name": "France",
    "code": "FR",
    "flagsImage": "~/assets/images/flags/fr.png"
  },
  {
    "name": "French Guiana",
    "code": "GF",
    "flagsImage": "~/assets/images/flags/gf.png"
  },
  {
    "name": "French Polynesia",
    "code": "PF",
    "flagsImage": "~/assets/images/flags/pf.png"
  },
  {
    "name": "French Southern Territories",
    "code": "TF",
    "flagsImage": "~/assets/images/flags/tf.png"
  },
  {
    "name": "Gabon",
    "code": "GA",
    "flagsImage": "~/assets/images/flags/ga.png"
  },
  {
    "name": "Gambia",
    "code": "GM",
    "flagsImage": "~/assets/images/flags/gm.png"
  },
  {
    "name": "Georgia",
    "code": "GE",
    "flagsImage": "~/assets/images/flags/ge.png"
  },
  {
    "name": "Germany",
    "code": "DE",
    "flagsImage": "~/assets/images/flags/de.png"
  },
  {
    "name": "Ghana",
    "code": "GH",
    "flagsImage": "~/assets/images/flags/gh.png"
  },
  {
    "name": "Gibraltar",
    "code": "GI",
    "flagsImage": "~/assets/images/flags/gi.png"
  },
  {
    "name": "Greece",
    "code": "GR",
    "flagsImage": "~/assets/images/flags/gr.png"
  },
  {
    "name": "Greenland",
    "code": "GL",
    "flagsImage": "~/assets/images/flags/gl.png"
  },
  {
    "name": "Grenada",
    "code": "GD",
    "flagsImage": "~/assets/images/flags/gd.png"
  },
  {
    "name": "Guadeloupe",
    "code": "GP",
    "flagsImage": "~/assets/images/flags/gp.png"
  },
  {
    "name": "Guam",
    "code": "GU",
    "flagsImage": "~/assets/images/flags/gu.png"
  },
  {
    "name": "Guatemala",
    "code": "GT",
    "flagsImage": "~/assets/images/flags/gt.png"
  },
  {
    "name": "Guernsey",
    "code": "GG",
    "flagsImage": "~/assets/images/flags/gg.png"
  },
  {
    "name": "Guinea",
    "code": "GN",
    "flagsImage": "~/assets/images/flags/gn.png"
  },
  {
    "name": "Guinea-Bissau",
    "code": "GW",
    "flagsImage": "~/assets/images/flags/gw.png"
  },
  {
    "name": "Guyana",
    "code": "GY",
    "flagsImage": "~/assets/images/flags/gy.png"
  },
  {
    "name": "Haiti",
    "code": "HT",
    "flagsImage": "~/assets/images/flags/ht.png"
  },
  {
    "name": "Heard Island and Mcdonald Islands",
    "code": "HM",
    "flagsImage": "~/assets/images/flags/hm.png"
  },
  {
    "name": "Holy See (Vatican City State)",
    "code": "VA",
    "flagsImage": "~/assets/images/flags/va.png"
  },
  {
    "name": "Honduras",
    "code": "HN",
    "flagsImage": "~/assets/images/flags/hn.png"
  },
  {
    "name": "Hong Kong",
    "code": "HK",
    "flagsImage": "~/assets/images/flags/hk.png"
  },
  {
    "name": "Hungary",
    "code": "HU",
    "flagsImage": "~/assets/images/flags/hu.png"
  },
  {
    "name": "Iceland",
    "code": "IS",
    "flagsImage": "~/assets/images/flags/is.png"
  },
  {
    "name": "India",
    "code": "IN",
    "flagsImage": "~/assets/images/flags/in.png"
  },
  {
    "name": "Indonesia",
    "code": "ID",
    "flagsImage": "~/assets/images/flags/id.png"
  },
  {
    "name": "Iran, Islamic Republic Of",
    "code": "IR",
    "flagsImage": "~/assets/images/flags/ir.png"
  },
  {
    "name": "Iraq",
    "code": "IQ",
    "flagsImage": "~/assets/images/flags/iq.png"
  },
  {
    "name": "Ireland",
    "code": "IE",
    "flagsImage": "~/assets/images/flags/ie.png"
  },
  {
    "name": "Isle of Man",
    "code": "IM",
    "flagsImage": "~/assets/images/flags/im.png"
  },
  {
    "name": "Israel",
    "code": "IL",
    "flagsImage": "~/assets/images/flags/il.png"
  },
  {
    "name": "Italy",
    "code": "IT",
    "flagsImage": "~/assets/images/flags/it.png"
  },
  {
    "name": "Jamaica",
    "code": "JM",
    "flagsImage": "~/assets/images/flags/jm.png"
  },
  {
    "name": "Japan",
    "code": "JP",
    "flagsImage": "~/assets/images/flags/jp.png"
  },
  {
    "name": "Jersey",
    "code": "JE",
    "flagsImage": "~/assets/images/flags/je.png"
  },
  {
    "name": "Jordan",
    "code": "JO",
    "flagsImage": "~/assets/images/flags/jo.png"
  },
  {
    "name": "Kazakhstan",
    "code": "KZ",
    "flagsImage": "~/assets/images/flags/kz.png"
  },
  {
    "name": "Kenya",
    "code": "KE",
    "flagsImage": "~/assets/images/flags/ke.png"
  },
  {
    "name": "Kiribati",
    "code": "KI",
    "flagsImage": "~/assets/images/flags/ki.png"
  },
  {
    "name": "Korea, Democratic People\"S Republic of",
    "code": "KP",
    "flagsImage": "~/assets/images/flags/kp.png"
  },
  {
    "name": "Korea, Republic of",
    "code": "KR",
    "flagsImage": "~/assets/images/flags/kr.png"
  },
  {
    "name": "Kuwait",
    "code": "KW",
    "flagsImage": "~/assets/images/flags/kw.png"
  },
  {
    "name": "Kyrgyzstan",
    "code": "KG",
    "flagsImage": "~/assets/images/flags/kg.png"
  },
  {
    "name": "Lao People\"S Democratic Republic",
    "code": "LA",
    "flagsImage": "~/assets/images/flags/la.png"
  },
  {
    "name": "Latvia",
    "code": "LV",
    "flagsImage": "~/assets/images/flags/lv.png"
  },
  {
    "name": "Lebanon",
    "code": "LB",
    "flagsImage": "~/assets/images/flags/lb.png"
  },
  {
    "name": "Lesotho",
    "code": "LS",
    "flagsImage": "~/assets/images/flags/ls.png"
  },
  {
    "name": "Liberia",
    "code": "LR",
    "flagsImage": "~/assets/images/flags/lr.png"
  },
  {
    "name": "Libyan Arab Jamahiriya",
    "code": "LY",
    "flagsImage": "~/assets/images/flags/ly.png"
  },
  {
    "name": "Liechtenstein",
    "code": "LI",
    "flagsImage": "~/assets/images/flags/li.png"
  },
  {
    "name": "Lithuania",
    "code": "LT",
    "flagsImage": "~/assets/images/flags/lt.png"
  },
  {
    "name": "Luxembourg",
    "code": "LU",
    "flagsImage": "~/assets/images/flags/lu.png"
  },
  {
    "name": "Macao",
    "code": "MO",
    "flagsImage": "~/assets/images/flags/mo.png"
  },
  {
    "name": "Macedonia, The Former Yugoslav Republic of",
    "code": "MK",
    "flagsImage": "~/assets/images/flags/mk.png"
  },
  {
    "name": "Madagascar",
    "code": "MG",
    "flagsImage": "~/assets/images/flags/mg.png"
  },
  {
    "name": "Malawi",
    "code": "MW",
    "flagsImage": "~/assets/images/flags/mw.png"
  },
  {
    "name": "Malaysia",
    "code": "MY",
    "flagsImage": "~/assets/images/flags/my.png"
  },
  {
    "name": "Maldives",
    "code": "MV",
    "flagsImage": "~/assets/images/flags/mv.png"
  },
  {
    "name": "Mali",
    "code": "ML",
    "flagsImage": "~/assets/images/flags/ml.png"
  },
  {
    "name": "Malta",
    "code": "MT",
    "flagsImage": "~/assets/images/flags/mt.png"
  },
  {
    "name": "Marshall Islands",
    "code": "MH",
    "flagsImage": "~/assets/images/flags/mh.png"
  },
  {
    "name": "Martinique",
    "code": "MQ",
    "flagsImage": "~/assets/images/flags/mq.png"
  },
  {
    "name": "Mauritania",
    "code": "MR",
    "flagsImage": "~/assets/images/flags/mr.png"
  },
  {
    "name": "Mauritius",
    "code": "MU",
    "flagsImage": "~/assets/images/flags/mu.png"
  },
  {
    "name": "Mayotte",
    "code": "YT",
    "flagsImage": "~/assets/images/flags/yt.png"
  },
  {
    "name": "Mexico",
    "code": "MX",
    "flagsImage": "~/assets/images/flags/mx.png"
  },
  {
    "name": "Micronesia, Federated States of",
    "code": "FM",
    "flagsImage": "~/assets/images/flags/fm.png"
  },
  {
    "name": "Moldova, Republic of",
    "code": "MD",
    "flagsImage": "~/assets/images/flags/md.png"
  },
  {
    "name": "Monaco",
    "code": "MC",
    "flagsImage": "~/assets/images/flags/mc.png"
  },
  {
    "name": "Mongolia",
    "code": "MN",
    "flagsImage": "~/assets/images/flags/mn.png"
  },
  {
    "name": "Montserrat",
    "code": "MS",
    "flagsImage": "~/assets/images/flags/ms.png"
  },
  {
    "name": "Morocco",
    "code": "MA",
    "flagsImage": "~/assets/images/flags/ma.png"
  },
  {
    "name": "Mozambique",
    "code": "MZ",
    "flagsImage": "~/assets/images/flags/mz.png"
  },
  {
    "name": "Myanmar",
    "code": "MM",
    "flagsImage": "~/assets/images/flags/mm.png"
  },
  {
    "name": "Namibia",
    "code": "NA",
    "flagsImage": "~/assets/images/flags/na.png"
  },
  {
    "name": "Nauru",
    "code": "NR",
    "flagsImage": "~/assets/images/flags/nr.png"
  },
  {
    "name": "Nepal",
    "code": "NP",
    "flagsImage": "~/assets/images/flags/np.png"
  },
  {
    "name": "Netherlands",
    "code": "NL",
    "flagsImage": "~/assets/images/flags/nl.png"
  },
  {
    "name": "Netherlands Antilles",
    "code": "AN",
    "flagsImage": "~/assets/images/flags/an.png"
  },
  {
    "name": "New Caledonia",
    "code": "NC",
    "flagsImage": "~/assets/images/flags/nc.png"
  },
  {
    "name": "New Zealand",
    "code": "NZ",
    "flagsImage": "~/assets/images/flags/nz.png"
  },
  {
    "name": "Nicaragua",
    "code": "NI",
    "flagsImage": "~/assets/images/flags/ni.png"
  },
  {
    "name": "Niger",
    "code": "NE",
    "flagsImage": "~/assets/images/flags/ne.png"
  },
  {
    "name": "Nigeria",
    "code": "NG",
    "flagsImage": "~/assets/images/flags/ng.png"
  },
  {
    "name": "Niue",
    "code": "NU",
    "flagsImage": "~/assets/images/flags/nu.png"
  },
  {
    "name": "Norfolk Island",
    "code": "NF",
    "flagsImage": "~/assets/images/flags/nf.png"
  },
  {
    "name": "Northern Mariana Islands",
    "code": "MP",
    "flagsImage": "~/assets/images/flags/mp.png"
  },
  {
    "name": "Norway",
    "code": "NO",
    "flagsImage": "~/assets/images/flags/no.png"
  },
  {
    "name": "Oman",
    "code": "OM",
    "flagsImage": "~/assets/images/flags/om.png"
  },
  {
    "name": "Pakistan",
    "code": "PK",
    "flagsImage": "~/assets/images/flags/pk.png"
  },
  {
    "name": "Palau",
    "code": "PW",
    "flagsImage": "~/assets/images/flags/pw.png"
  },
  {
    "name": "Palestinian Territory, Occupied",
    "code": "PS",
    "flagsImage": "~/assets/images/flags/ps.png"
  },
  {
    "name": "Panama",
    "code": "PA",
    "flagsImage": "~/assets/images/flags/pa.png"
  },
  {
    "name": "Papua New Guinea",
    "code": "PG",
    "flagsImage": "~/assets/images/flags/pg.png"
  },
  {
    "name": "Paraguay",
    "code": "PY",
    "flagsImage": "~/assets/images/flags/py.png"
  },
  {
    "name": "Peru",
    "code": "PE",
    "flagsImage": "~/assets/images/flags/pe.png"
  },
  {
    "name": "Philippines",
    "code": "PH",
    "flagsImage": "~/assets/images/flags/ph.png"
  },
  {
    "name": "Pitcairn",
    "code": "PN",
    "flagsImage": "~/assets/images/flags/pn.png"
  },
  {
    "name": "Poland",
    "code": "PL",
    "flagsImage": "~/assets/images/flags/pl.png"
  },
  {
    "name": "Portugal",
    "code": "PT",
    "flagsImage": "~/assets/images/flags/pt.png"
  },
  {
    "name": "Puerto Rico",
    "code": "PR",
    "flagsImage": "~/assets/images/flags/pr.png"
  },
  {
    "name": "Qatar",
    "code": "QA",
    "flagsImage": "~/assets/images/flags/qa.png"
  },
  {
    "name": "Reunion",
    "code": "RE",
    "flagsImage": "~/assets/images/flags/re.png"
  },
  {
    "name": "Romania",
    "code": "RO",
    "flagsImage": "~/assets/images/flags/ro.png"
  },
  {
    "name": "Russian Federation",
    "code": "RU",
    "flagsImage": "~/assets/images/flags/ru.png"
  },
  {
    "name": "RWANDA",
    "code": "RW",
    "flagsImage": "~/assets/images/flags/rw.png"
  },
  {
    "name": "Saint Helena",
    "code": "SH",
    "flagsImage": "~/assets/images/flags/sh.png"
  },
  {
    "name": "Saint Kitts and Nevis",
    "code": "KN",
    "flagsImage": "~/assets/images/flags/kn.png"
  },
  {
    "name": "Saint Lucia",
    "code": "LC",
    "flagsImage": "~/assets/images/flags/lc.png"
  },
  {
    "name": "Saint Pierre and Miquelon",
    "code": "PM",
    "flagsImage": "~/assets/images/flags/pm.png"
  },
  {
    "name": "Saint Vincent and the Grenadines",
    "code": "VC",
    "flagsImage": "~/assets/images/flags/vc.png"
  },
  {
    "name": "Samoa",
    "code": "WS",
    "flagsImage": "~/assets/images/flags/ws.png"
  },
  {
    "name": "San Marino",
    "code": "SM",
    "flagsImage": "~/assets/images/flags/sm.png"
  },
  {
    "name": "Sao Tome and Principe",
    "code": "ST",
    "flagsImage": "~/assets/images/flags/st.png"
  },
  {
    "name": "Saudi Arabia",
    "code": "SA",
    "flagsImage": "~/assets/images/flags/sa.png"
  },
  {
    "name": "Senegal",
    "code": "SN",
    "flagsImage": "~/assets/images/flags/sn.png"
  },
  {
    "name": "Serbia and Montenegro",
    "code": "CS",
    "flagsImage": "~/assets/images/flags/cs.png"
  },
  {
    "name": "Seychelles",
    "code": "SC",
    "flagsImage": "~/assets/images/flags/sc.png"
  },
  {
    "name": "Sierra Leone",
    "code": "SL",
    "flagsImage": "~/assets/images/flags/sl.png"
  },
  {
    "name": "Singapore",
    "code": "SG",
    "flagsImage": "~/assets/images/flags/sg.png"
  },
  {
    "name": "Slovakia",
    "code": "SK",
    "flagsImage": "~/assets/images/flags/sk.png"
  },
  {
    "name": "Slovenia",
    "code": "SI",
    "flagsImage": "~/assets/images/flags/si.png"
  },
  {
    "name": "Solomon Islands",
    "code": "SB",
    "flagsImage": "~/assets/images/flags/sb.png"
  },
  {
    "name": "Somalia",
    "code": "SO",
    "flagsImage": "~/assets/images/flags/so.png"
  },
  {
    "name": "South Africa",
    "code": "ZA",
    "flagsImage": "~/assets/images/flags/za.png"
  },
  {
    "name": "South Georgia and the South Sandwich Islands",
    "code": "GS",
    "flagsImage": "~/assets/images/flags/gs.png"
  },
  {
    "name": "Spain",
    "code": "ES",
    "flagsImage": "~/assets/images/flags/es.png"
  },
  {
    "name": "Sri Lanka",
    "code": "LK",
    "flagsImage": "~/assets/images/flags/lk.png"
  },
  {
    "name": "Sudan",
    "code": "SD",
    "flagsImage": "~/assets/images/flags/sd.png"
  },
  {
    "name": "Suriname",
    "code": "SR",
    "flagsImage": "~/assets/images/flags/sr.png"
  },
  {
    "name": "Svalbard and Jan Mayen",
    "code": "SJ",
    "flagsImage": "~/assets/images/flags/sj.png"
  },
  {
    "name": "Swaziland",
    "code": "SZ",
    "flagsImage": "~/assets/images/flags/sz.png"
  },
  {
    "name": "Sweden",
    "code": "SE",
    "flagsImage": "~/assets/images/flags/se.png"
  },
  {
    "name": "Switzerland",
    "code": "CH",
    "flagsImage": "~/assets/images/flags/ch.png"
  },
  {
    "name": "Syrian Arab Republic",
    "code": "SY",
    "flagsImage": "~/assets/images/flags/sy.png"
  },
  {
    "name": "Taiwan, Province of China",
    "code": "TW",
    "flagsImage": "~/assets/images/flags/tw.png"
  },
  {
    "name": "Tajikistan",
    "code": "TJ",
    "flagsImage": "~/assets/images/flags/tj.png"
  },
  {
    "name": "Tanzania, United Republic of",
    "code": "TZ",
    "flagsImage": "~/assets/images/flags/tz.png"
  },
  {
    "name": "Thailand",
    "code": "TH",
    "flagsImage": "~/assets/images/flags/th.png"
  },
  {
    "name": "Timor-Leste",
    "code": "TL",
    "flagsImage": "~/assets/images/flags/tl.png"
  },
  {
    "name": "Togo",
    "code": "TG",
    "flagsImage": "~/assets/images/flags/tg.png"
  },
  {
    "name": "Tokelau",
    "code": "TK",
    "flagsImage": "~/assets/images/flags/tk.png"
  },
  {
    "name": "Tonga",
    "code": "TO",
    "flagsImage": "~/assets/images/flags/to.png"
  },
  {
    "name": "Trinidad and Tobago",
    "code": "TT",
    "flagsImage": "~/assets/images/flags/tt.png"
  },
  {
    "name": "Tunisia",
    "code": "TN",
    "flagsImage": "~/assets/images/flags/tn.png"
  },
  {
    "name": "Turkey",
    "code": "TR",
    "flagsImage": "~/assets/images/flags/tr.png"
  },
  {
    "name": "Turkmenistan",
    "code": "TM",
    "flagsImage": "~/assets/images/flags/tm.png"
  },
  {
    "name": "Turks and Caicos Islands",
    "code": "TC",
    "flagsImage": "~/assets/images/flags/tc.png"
  },
  {
    "name": "Tuvalu",
    "code": "TV",
    "flagsImage": "~/assets/images/flags/tv.png"
  },
  {
    "name": "Uganda",
    "code": "UG",
    "flagsImage": "~/assets/images/flags/ug.png"
  },
  {
    "name": "Ukraine",
    "code": "UA",
    "flagsImage": "~/assets/images/flags/ua.png"
  },
  {
    "name": "United Arab Emirates",
    "code": "AE",
    "flagsImage": "~/assets/images/flags/ae.png"
  },
  {
    "name": "United Kingdom",
    "code": "GB",
    "flagsImage": "~/assets/images/flags/gb.png"
  },
  {
    "name": "United States",
    "code": "US",
    "flagsImage": "~/assets/images/flags/us.png"
  },
  {
    "name": "United States Minor Outlying Islands",
    "code": "UM",
    "flagsImage": "~/assets/images/flags/um.png"
  },
  {
    "name": "Uruguay",
    "code": "UY",
    "flagsImage": "~/assets/images/flags/uy.png"
  },
  {
    "name": "Uzbekistan",
    "code": "UZ",
    "flagsImage": "~/assets/images/flags/uz.png"
  },
  {
    "name": "Vanuatu",
    "code": "VU",
    "flagsImage": "~/assets/images/flags/vu.png"
  },
  {
    "name": "Venezuela",
    "code": "VE",
    "flagsImage": "~/assets/images/flags/ve.png"
  },
  {
    "name": "Viet Nam",
    "code": "VN",
    "flagsImage": "~/assets/images/flags/vn.png"
  },
  {
    "name": "Virgin Islands, British",
    "code": "VG",
    "flagsImage": "~/assets/images/flags/vg.png"
  },
  {
    "name": "Virgin Islands, U.S.",
    "code": "VI",
    "flagsImage": "~/assets/images/flags/vi.png"
  },
  {
    "name": "Wallis and Futuna",
    "code": "WF",
    "flagsImage": "~/assets/images/flags/wf.png"
  },
  {
    "name": "Western Sahara",
    "code": "EH",
    "flagsImage": "~/assets/images/flags/eh.png"
  },
  {
    "name": "Yemen",
    "code": "YE",
    "flagsImage": "~/assets/images/flags/ye.png"
  },
  {
    "name": "Zambia",
    "code": "ZM",
    "flagsImage": "~/assets/images/flags/zm.png"
  },
  {
    "name": "Zimbabwe",
    "code": "ZW",
    "flagsImage": "~/assets/images/flags/zw.png"
  }
]