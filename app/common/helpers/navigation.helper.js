let contentFrame = null;

export const setContentFrame = (frame) => {
  contentFrame = frame;
};

export const getContentFrame = () => contentFrame;
