export const toUpperCase = (text) => `${text || ''}`.toUpperCase();
export const toLowerCase = (text) => `${text || ''}`.toLowerCase();