
var application = require("tns-core-modules/application/application");

function getAppDelegate() {
    if (application.ios.delegate === undefined) {
        var UIApplicationDelegateImpl = (function (_super) {
            __extends(UIApplicationDelegateImpl, _super);
            function UIApplicationDelegateImpl() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            UIApplicationDelegateImpl = __decorate([
                ObjCClass(UIApplicationDelegate)
            ], UIApplicationDelegateImpl);
            return UIApplicationDelegateImpl;
        }(UIResponder));
        application.ios.delegate = UIApplicationDelegateImpl;
    }
    return application.ios.delegate;
}

function addAppDelegateMethods (appDelegate) {

    appDelegate.prototype.applicationDidFinishLaunchingWithOptions = function (application, launchOptions) {
        if (launchOptions) {
            var remoteNotification = launchOptions.objectForKey(UIApplicationLaunchOptionsRemoteNotificationKey);
            if (remoteNotification) {
                firebaseMessaging.handleRemoteNotification(application, remoteNotification);
            }
        }
        if (typeof (FBSDKApplicationDelegate) !== "undefined") {
            FBSDKApplicationDelegate.sharedInstance.applicationDidFinishLaunchingWithOptions(application, launchOptions);
        }
        return true;
    };

    appDelegate.prototype.applicationOpenURLSourceApplicationAnnotation = function (application, url, sourceApplication, annotation) {
        var result = false;
        if (typeof (FBSDKApplicationDelegate) !== "undefined") {
            result = FBSDKApplicationDelegate.sharedInstance.applicationOpenURLSourceApplicationAnnotation(application, url, sourceApplication, annotation);
        }
        return result;
    };

    appDelegate.prototype.applicationOpenURLOptions = function (application, url, options) {
        var result = false;
        if (typeof (FBSDKApplicationDelegate) !== "undefined") {
            result = FBSDKApplicationDelegate.sharedInstance.applicationOpenURLSourceApplicationAnnotation(application, url, options.valueForKey(UIApplicationOpenURLOptionsSourceApplicationKey), options.valueForKey(UIApplicationOpenURLOptionsAnnotationKey));
        }
        return result;
    };

};

export var fbsdk = {
    initial: function () {
        console.log("INIT FB SDK")
        addAppDelegateMethods(getAppDelegate());
    }
}


// application.run({ moduleName: "app-root" });
